package com.crecolto.hupaypos;

import android.os.Bundle;

import androidx.annotation.Nullable;

public class ResultActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
    }
}
