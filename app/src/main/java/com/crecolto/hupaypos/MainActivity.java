package com.crecolto.hupaypos;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.crecolto.hupaypos.utils.AppUtil;
import com.crecolto.hupaypos.utils.CipherUtils;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    LinearLayout payment_qr_code, payment_card, payment_type_selector;
    FrameLayout qr_code;
    Button btn_card, btn_qr_code;
    ImageView btn_cancel;
    TextView user_team, user_code, user_name, kind_code, kind_code_01;
    TextView user_price;
    private final String KIS_MOBILE = "com.kismobile";
    private String partnerId = "KIS1234567890";  // 임의값 변경 필요 -> 가맹점의 고객별 ID (30 bytes 초과시 오류 발생)
    private String partnerCd = "BAA91";          // 전달된 가맹점(매장)별 파트너 코드 사용 테스트용 : BAA91
    private String merchantCd = "8758701122";    // 파트너코드 발급 시 등록한 사업자번호 사용 테스트용 : 116-81-43939
    private final String vanId = "000002";
    private String otc;
    private String price = "1004";   // 결제 금액
    private String authNum;
    private String authDate;
    private JSONObject userData = null;

//    new data 20191114
//    사업자번호 : 8758701122
//    일련번호 : 1
//    패스워드 : 6844

//    private String requestPaymentByAppCard = "niceappcard://payment?partner_cd=SAMPLEPARTNERCODE&partner_id=SAMPLE_PARTNER_ID" +
//            "&merchant_cd=암호화된사업자번호&pay_order=A&callback=startActivityForResult&payPrice=PRICE&part ner_rgb=RGBCODE&h=SAMPLE_HASH";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        payment_type_selector = (LinearLayout) findViewById(R.id.payment_type_selector);
        payment_card = (LinearLayout) findViewById(R.id.payment_card);
        payment_qr_code = (LinearLayout) findViewById(R.id.payment_qr_code);
        btn_card = (Button) findViewById(R.id.btn_card);
        btn_qr_code = (Button) findViewById(R.id.btn_qr_code);
        btn_cancel = (ImageView) findViewById(R.id.btn_cancel);
        user_team = (TextView) findViewById(R.id.user_team);
        kind_code = (TextView) findViewById(R.id.kind_code);
        kind_code_01 = (TextView) findViewById(R.id.kind_code_01);
        user_code = (TextView) findViewById(R.id.user_code);
        user_price = (TextView) findViewById(R.id.user_price);
        user_name = (TextView) findViewById(R.id.user_name);
        qr_code = (FrameLayout) findViewById(R.id.qr_code);
        btn_card.setOnClickListener(this);
        btn_qr_code.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);

        viewSelector(99);

        try {

            AppUtil.setUserData(new JSONObject(loadUserData()).getJSONObject("user"));

            if(AppUtil.getUserData() != null){

                setUserData();

            }else{

                Toast.makeText(this, "user data not loaded", Toast.LENGTH_SHORT).show();

            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*IntentFilter filter = new IntentFilter();
        filter.addAction(action);
        registerReceiver(br, filter);
        isRegistBr = true;

        Log.d(LOG, "registered br");*/

        registerReceiver(broadcastReceiver, new IntentFilter(Intent.ACTION_MAIN));
        isRegistBr = true;

    }

    private void setUserData(){

        try {
            user_name.setText(AppUtil.getUserName());
            user_code.setText(AppUtil.getUserCode());
            user_team.setText(AppUtil.getUserTeam());
            kind_code.setText(AppUtil.getMoneyType());
            kind_code_01.setText(AppUtil.getMoneyType());
            user_price.setText(AppUtil.getPrice());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private final boolean isInstallKisMobile() {
        PackageManager pm = this.getPackageManager();

        try {
            pm.getApplicationInfo(this.KIS_MOBILE, PackageManager.GET_META_DATA);
            return true;
        } catch (PackageManager.NameNotFoundException var3) {
            return false;
        }
    }

    private String loadUserData() throws IOException {

        InputStream is = getResources().openRawResource(R.raw.user_payment_dummy);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];

        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1){

                writer.write(buffer, 0, n);

            }
        } finally {
            is.close();
        }

        String jStr = writer.toString();

        return jStr;

    }

    public final String TAG = "RECEIVE_RESULT_DATA";
    private String callMode = "";

    //    new data 20191114
    //    사업자번호 : 8758701122
    //    일련번호 : 1
    //    패스워드 : 6844

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @SuppressWarnings("unchecked")
        @Override
        public void onReceive(Context context, Intent intent) {

            Log.e(TAG,"Broadcast Receiving....................."+intent.hashCode()+"\n"+intent.toUri(0));

            try{
                HashMap<String, String> response = (HashMap<String, String>)intent.getSerializableExtra("result");
                Iterator<String> iterator = response.keySet().iterator();
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();
                    String val = response.get(key);
                    if (val != null) val = val.trim();
                    Log.e(TAG, "RECEIVER key = " + key + ",   value = "+val);
                }

                Log.e(TAG,response.toString());

                String code = response.get("outReplyCode");
                if (code != null && code.equals("0000") && callMode.equalsIgnoreCase("pay")) {

                    // 취소 정보가 있을 시 취소

                    final String approvalNo = response.get("outAuthNo");
                    final String approvalDate = response.get("outAuthDate");

                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle(getResources().getString(R.string.txt_item_error))
                            .setMessage("승인번호 ["+approvalNo+"]의 결제를 취소 합니다")

                            // Specifying a listener allows you to take an action before dismissing the dialog.
                            // The dialog is automatically dismissed when a dialog button is clicked.
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Continue with delete operation

                                    callMode = "cancel";
                                    Intent intent = new Intent(action);
                                    intent.putExtra("uuid",String.valueOf(System.currentTimeMillis()));
                                    intent.putExtra("bizNo","1168143939");
                                    intent.putExtra("serialNo","1");
                                    intent.putExtra("downPasswordNo","6844");
                                    intent.putExtra("type","card");
                                    intent.putExtra("mode",callMode);
                                    intent.putExtra("approvalNo",approvalNo);
                                    intent.putExtra("approvalDate",approvalDate);

                                    /*intent.putExtra("uuid", String.valueOf(System.currentTimeMillis()));
                                    intent.putExtra("bizNo", "1168143939");
                                    intent.putExtra("serialNo", "13");
                                    intent.putExtra("downPasswordNo", "0054");
                                    intent.putExtra("localNo", "02");
                                    intent.putExtra("type", "card");
                                    intent.putExtra("mode", "pay");
                                    intent.putExtra("amount", price);
                                    intent.putExtra("taxFree", 0);
                                    intent.putExtra("fee", 0);
                                    intent.putExtra("installment", 0);*/

                                    startActivity(intent);

                                    dialog.dismiss();
                                }
                            }).show();

                }

            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
    };

    public final String LOG = "RESULT_DATA";
    private boolean isRegistBr = false;

    /*private BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(action)){

                Log.d(LOG, "************************************* result data receive **************************************\n" + "ACTION : " + intent.getAction());

                paymentResult(1, "payment_cancel_by_user");

                *//*Serializable resultData = intent.getSerializableExtra("result");
                HashMap <String, String> response = (HashMap)resultData;
                Iterator iterator = response.keySet().iterator();

                while(iterator.hasNext()) {

                    String strKey = (String) iterator.next();
                    String currentData = (String)response.get(strKey);

                    Log.d(LOG, "\nKEY : " + strKey +"\nDATA : " + currentData);

                }*//*

            }

            *//**
             * unregister receiver
             *//*

            Log.d(LOG, "************************************* the end of result data **************************************\n");

            if(isRegistBr){

                unregisterReceiver(br);
                isRegistBr = false;

            }

        }
    };*/

    String action = "com.kismobile.pay";

    private final void checkKisMobile() {
        Intent intent;
        if (this.isInstallKisMobile()) {

//            String action = "com.kismobile.pay";
            intent = new Intent(action);

            if(user_price.getText().toString().equals("")){

                new AlertDialog.Builder(this)
                        .setTitle(getResources().getString(R.string.txt_item_error))
                        .setMessage(getResources().getString(R.string.txt_item_error_price))

                        // Specifying a listener allows you to take an action before dismissing the dialog.
                        // The dialog is automatically dismissed when a dialog button is clicked.
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Continue with delete operation
                                viewSelector(99);
                                dialog.dismiss();
                            }
                        }).show();
                return;

            }

            int temp_price = Integer.parseInt(user_price.getText().toString());

            if(temp_price < 9999/* && temp_price < 500001 && temp_price % 10000 == 0*/){

                /**
                 * register receiver
                 */

                //    new data 20191114
                //    사업자번호 : 8758701122
                //    일련번호 : 1
                //    패스워드 : 6844

                /*IntentFilter filter = new IntentFilter();
                filter.addAction(action);
                registerReceiver(broadcastReceiver, filter);
                isRegistBr = true;*/

//                Log.d(TAG, "registered br");

                int price = 50001;/*Integer.parseInt(user_price.getText().toString());*/
//                String var4 = "price : " + price;
//                boolean var5 = false;
//                System.out.println(var4);
                intent.putExtra("uuid", String.valueOf(System.currentTimeMillis()));
                intent.putExtra("bizNo", "8758701122");
                intent.putExtra("serialNo", "1");
                intent.putExtra("downPasswordNo", "6844");
                intent.putExtra("localNo", "02");
                intent.putExtra("type", "card");
                intent.putExtra("mode", "pay");
                intent.putExtra("amount", price);
                intent.putExtra("taxFree", 0);
                intent.putExtra("fee", 0);
                intent.putExtra("installment", 0);
                this.startActivity(intent);

//                Intent intent = new Intent(Intent.ACTION_VIEW);
//                intent.addCategory(Intent.CATEGORY_DEFAULT);
                /*String systemTime = getSystemDate();
                String hPartnerId = partnerId + systemTime;
                String hMerchantCd = merchantCd + systemTime;
                String hVanId     = vanId + systemTime;
                String encryptPartnerId = "";
                String encryptMerchantCd = "";
                String encryptVanId = "";

                try {
                    encryptPartnerId = getH(hPartnerId, "ceappcardswallet");
                    encryptMerchantCd = getH(hMerchantCd, "ceappcardswallet");
                    encryptVanId = getH(hVanId, "ceappcardswallet");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String encryptSHA = getParamHash256(partnerCd + partnerId);



                String uri = "niceappcard://payment?" +
                        "partner_cd=" + partnerCd +
                        "&partner_id=" + encryptPartnerId +
                        "&van_id=" + encryptVanId +
                        "&merchant_cd=" + encryptMerchantCd +
                        "&pay_order=A" +         // App to App 응답 필수값
                        "&payPrice=" + String.valueOf(temp_price) +   // 5만원 이상 서명 확인
                        "&partner_rgb=FF0000" +  // 제휴사 색상값 변경가능
                        "&h=" + encryptSHA*//* + // 필수 값
                                "&posIp=" + getPublicIPAddress(this)*//*;*/

            }else{

                new AlertDialog.Builder(this)
                        .setTitle(getResources().getString(R.string.txt_item_not_found_kis_title))
                        .setMessage(getResources().getString(R.string.txt_item_error_price_mix_max))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();

            }

        } else {

            new AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.txt_item_not_found_kis_title))
                    .setMessage(getResources().getString(R.string.txt_item_not_found_kis_content))
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            installKIS();
                            dialog.dismiss();
                        }
                    }).show();

        }

    }

    private void installKIS(){

        Uri uri = Uri.parse("market://details?id=" + this.KIS_MOBILE);
        Intent intent = new Intent("android.intent.action.VIEW", uri);
        this.startActivity(intent);

    }

    /**
     * QR code generator
     * @param contents
     */

    public void generateRQCode(String contents) {

        Log.d("SCAN_DATA", "DATA : " + contents);

        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        try {
            Bitmap bitmap = toBitmap(qrCodeWriter.encode(contents, BarcodeFormat.QR_CODE, 300, 300));
            ImageView iv = new ImageView(this);
            iv.setImageBitmap(bitmap);
            qr_code.removeAllViews();
            qr_code.addView(iv);

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap toBitmap(BitMatrix matrix) {
        int height = matrix.getHeight();
        int width = matrix.getWidth();
        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                bmp.setPixel(x, y, matrix.get(x, y) ? Color.BLACK : Color.WHITE);
            }
        }
        return bmp;
    }

    private static String getH(String parameters, String key) throws Exception {
        CipherUtils cu = new CipherUtils(key);
        String b = URLEncoder.encode(cu.encrypt(parameters), "UTF-8");
        return b;
    }

    public static String getSystemDate() {
        long time = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("hhmmss");
        Date dd = new Date(time);
        return sdf.format(dd);
    }

    public static String getParamHash256(String password){
        MessageDigest digest;
        StringBuffer sb = new StringBuffer();
        try {
            digest = MessageDigest.getInstance("SHA-256");
            digest.update(password.getBytes());
            byte[] input = digest.digest();
            for(int i = 0 ; i < input.length ; i++){
                sb.append(Integer.toString((input[i] & 0xff) + 0x100, 16).substring(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    private void viewSelector(int type){

        /**
         * 0 : payment by card
         * 1 : payment by qr code
         * 99 : reset
         */

        switch (type){

            case 0 :

                if(payment_card.getVisibility() != View.VISIBLE){

                    payment_type_selector.setVisibility(View.GONE);
                    payment_qr_code.setVisibility(View.GONE);
                    payment_card.setVisibility(View.VISIBLE);
                    checkKisMobile();

                }

                break;

            case 1 :

                if(user_price.getText().toString().equals("")){

                    new AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.txt_item_error))
                            .setMessage(getResources().getString(R.string.txt_item_error_price))

                            // Specifying a listener allows you to take an action before dismissing the dialog.
                            // The dialog is automatically dismissed when a dialog button is clicked.
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Continue with delete operation
                                    viewSelector(99);
                                    dialog.dismiss();
                                }
                            }).show();
                    return;

                }

                int temp_price = Integer.parseInt(user_price.getText().toString());

                if(temp_price < 9999 /*&& temp_price < 500001 && temp_price % 10000 == 0*/){

                    if(payment_qr_code.getVisibility() != View.VISIBLE){

                        payment_type_selector.setVisibility(View.GONE);
                        payment_qr_code.setVisibility(View.VISIBLE);
                        payment_card.setVisibility(View.GONE);

                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                        String systemTime = getSystemDate();
                        String hPartnerId = partnerId + systemTime;
                        String hMerchantCd = merchantCd + systemTime;
                        String hVanId     = vanId + systemTime;
                        String encryptPartnerId = "";
                        String encryptMerchantCd = "";
                        String encryptVanId = "";

                        try {
                            encryptPartnerId = getH(hPartnerId, "ceappcardswallet");
                            encryptMerchantCd = getH(hMerchantCd, "ceappcardswallet");
                            encryptVanId = getH(hVanId, "ceappcardswallet");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        String encryptSHA = getParamHash256(partnerCd + partnerId);



                        String uri = "niceappcard://payment?" +
                                "partner_cd=" + partnerCd +
                                "&partner_id=" + encryptPartnerId +
                                "&van_id=" + encryptVanId +
                                "&merchant_cd=" + encryptMerchantCd +
                                "&pay_order=A" +         // App to App 응답 필수값
                                "&payPrice=" + String.valueOf(temp_price) +   // 5만원 이상 서명 확인
                                "&partner_rgb=FF0000" +  // 제휴사 색상값 변경가능
                                "&h=" + encryptSHA/* + // 필수 값
                                "&posIp=" + getPublicIPAddress(this)*/;
//                        Log.d("kis", "uri = " + uri);
                        intent.setData(Uri.parse(uri));
//                    startActivityForResult(intent, 0);
                        Log.d("SCAN", "DATA : " + intent.toString());

//                        showProgress();
                        generateRQCode(/*intent.toString()*/uri);

                    }

                }else{

                    new AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.txt_item_not_found_kis_title))
                            .setMessage(getResources().getString(R.string.txt_item_error_price_mix_max))
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();

                }

                break;

            default:

                if(payment_type_selector.getVisibility() != View.VISIBLE){

                    payment_type_selector.setVisibility(View.VISIBLE);
                    payment_qr_code.setVisibility(View.GONE);
                    payment_card.setVisibility(View.GONE);

                }

        }

    }

    public static String getPublicIPAddress(Context context) {
        //final NetworkInfo info = NetworkUtils.getNetworkInfo(context);

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo info = cm.getActiveNetworkInfo();

        RunnableFuture<String> futureRun = new FutureTask<>(new Callable<String>() {
            @Override
            public String call() throws Exception {
                if ((info != null && info.isAvailable()) && (info.isConnected())) {
                    StringBuilder response = new StringBuilder();

                    try {
                        HttpURLConnection urlConnection = (HttpURLConnection) (
                                new URL("http://checkip.amazonaws.com/").openConnection());
                        urlConnection.setRequestProperty("User-Agent", "Android-device");
                        //urlConnection.setRequestProperty("Connection", "close");
                        urlConnection.setReadTimeout(15000);
                        urlConnection.setConnectTimeout(15000);
                        urlConnection.setRequestMethod("GET");
                        urlConnection.setRequestProperty("Content-type", "application/json");
                        urlConnection.connect();

                        int responseCode = urlConnection.getResponseCode();

                        if (responseCode == HttpURLConnection.HTTP_OK) {

                            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                            String line;
                            while ((line = reader.readLine()) != null) {
                                response.append(line);
                            }

                        }
                        urlConnection.disconnect();
                        return response.toString();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    //Log.w(TAG, "No network available INTERNET OFF!");
                    return null;
                }
                return null;
            }
        });

        new Thread(futureRun).start();

        try {
            return futureRun.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }

    }


    private final long FINISH_INTERVAL_TIME = 2000;
    private long backPressedTime = 0;

    @Override
    public void onBackPressed() {
        long tempTime = System.currentTimeMillis();
        long intervalTime = tempTime - backPressedTime;

        if (0 <= intervalTime && FINISH_INTERVAL_TIME >= intervalTime){

            super.onBackPressed();
            if(isRegistBr){

                unregisterReceiver(broadcastReceiver);
                isRegistBr = false;

            }

        }else{

            if(payment_type_selector.getVisibility() != View.VISIBLE){

                viewSelector(99);
                return;
            }

            backPressedTime = tempTime;
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.item_txt_exit), Toast.LENGTH_SHORT).show();

        }
    }

    private void paymentResult(final int resultCode, final String resultData){

        /**
         * 0 : success
         * 1 : fail
         */

        switch (resultCode){

            case 0 :

                showProgress();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        hideProgress();
                        viewSelector(99);
                        Intent i = new Intent(MainActivity.this, ResultActivity.class);
                        i.putExtra("resultCode", resultCode);
                        i.putExtra("resultData", resultData);
                        startActivity(i);
                        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

                    }
                }, 2000);

                break;

            case 1 :

                Toast.makeText(this, resultData, Toast.LENGTH_SHORT).show();

                break;

        }

    }

    private void showDialog(){

        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.txt_item_error))
                .setMessage(getResources().getString(R.string.txt_item_error_msg))

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation
                        viewSelector(99);
                        dialog.dismiss();
                    }
                }).show();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btn_card : viewSelector(0);break;

            case R.id.btn_qr_code : viewSelector(1);break;

            case R.id.btn_cancel : viewSelector(99);break;

            case R.id.user_team : paymentResult(0, "success");break;

            case R.id.user_code : /*showDialog()*/;
                sendBroadcast(new Intent(action));break;

        }

    }
}
