package com.crecolto.hupaypos;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Scanner;

import static com.crecolto.hupaypos.GlobalSocket.socketOut;

public class SplashActivity extends BaseActivity {

    public static final String TAG = "SPLASH_ACTIVITY";
    public static final int TIMEOUT = 60;
    private ServerSocket server = null;
    private String connectionStatus = null;
    private Handler mHandler = null;
    public Socket client = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        moveToMain();

//        this.mHandler = new Handler();
//        startSocket();

    }

    private void moveToMain(){

        showProgress();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                hideProgress();
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                finish();

            }
        }, 2000);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        /**
         * close server
         */

        /*try {
            server.close();
            Log.d(TAG, "Close server on destroy");
        } catch (IOException e) {
            e.printStackTrace();
        }*/

    }

    /**
     * socket
     */

    /*private Socket socket;
    BufferedReader in;
    String msg = "";

    public void startSocket() {

        Thread worker = new Thread() {
            public void run() {
                try {
                    socket = new Socket("192.168.0.68", 19999);
                    in = new BufferedReader(new InputStreamReader(
                            socket.getInputStream()));

                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    while (true) {
                        msg = in.readLine();
                        Log.d("GET_MSG", "DATA : " + msg);
                    }
                } catch (Exception e) {
                }
            }
        };
        worker.start();
    }*/

    /*@Override
    protected void onStop() {
        super.onStop();
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }*/

    /*private Runnable showConnectionStatus = new Runnable() {
        public void run() {
            Toast.makeText(SplashActivity.this, connectionStatus, Toast.LENGTH_SHORT).show();
        }
    };*/

}
